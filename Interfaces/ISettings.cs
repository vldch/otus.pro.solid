﻿namespace GuessGame_Solid.Interfaces
{
    public interface ISettings
    {
        public int Min { get; }
        public int Max { get; }
        public int AttemptsNumber { get; }

    }
}
