﻿using GuessGame_Solid.Classes;
using GuessGame_Solid.Interfaces;
using System;

namespace GuessGame_Solid
{
    public class Program
    {
        private static IGame _game;
        static void Main(string[] args)
        {
            _game = GameFactory.GetInstance();

            _game.Start();
        }
    }
}
