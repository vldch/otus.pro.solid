﻿using GuessGame_Solid.Interfaces;

namespace GuessGame_Solid.Classes
{
    public class Game : IGame
    {
        private readonly ISettings _settings;

        private readonly InputOutput _inputOutput;
        
        private int _generatedNumber;
        
        private int _attemptNumber = 0;

        public Game(ISettings settings, InputOutput inputOutput)
        {
            _settings = settings;

            _inputOutput = inputOutput;
        }
        public void Start()
        {
            _generatedNumber = new Generator(_settings, new RandomNumber()).GeneratedNumber;

            _inputOutput.Print($"Загаданное число: {_generatedNumber}");
           
            _inputOutput.Print(
                $"Отгадайте число от {_settings.Min} до {_settings.Max}, число попыток {_settings.AttemptsNumber}");

            while (_attemptNumber++ < _settings.AttemptsNumber)
            {
                var input = _inputOutput.ReadLine();

                var result = int.TryParse(input, out var number);

                if (result)
                {
                    if (ProcessNumber(number))
                        break;
                }
                else
                {
                    _inputOutput.Print(
                        $":-(, попробуйте еще раз, Вы ввели неправильное значение, осталось {_settings.AttemptsNumber - _attemptNumber} попыток");
                }
            }
        }

        private bool ProcessNumber(int number)
        {
            if (number == _generatedNumber)
            {
                _inputOutput.Print($"Вы угадали число с {_attemptNumber} попытки");
                return true;
            }

            if (number > _settings.Max || number < _settings.Min)
            {
                _inputOutput.Print(
                    $":-(, Ваше число вне диапазона, попробуйте еще раз, осталось {_settings.AttemptsNumber - _attemptNumber} попыток");
                return false;
            }

            _inputOutput.Print(
                $":-(, Ваше число {(number > _generatedNumber ? "больше" : "меньше")} загаданного, попробуйте еще раз, осталось {_settings.AttemptsNumber - _attemptNumber} попыток");
            
            return false;
        }
    }
}
