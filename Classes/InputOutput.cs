﻿using System;

namespace GuessGame_Solid.Interfaces
{
    public class InputOutput : IOutput, IInput
    {
        public virtual void Print(string str)
        {
            Console.WriteLine(str);
        }

        public virtual string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}
