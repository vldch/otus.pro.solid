﻿using GuessGame_Solid.Interfaces;

namespace GuessGame_Solid.Classes
{

    public class RandomNumber : IRandom
    {

        private readonly RandomChanged _rnd = new();

        public int Next(int from, int to)
        {

            return _rnd.Next(from, to);

        }
    }
}
