﻿namespace GuessGame_Solid.Interfaces
{
    public interface IRandom
    {
        public int Next(int from, int to);

    }
}
