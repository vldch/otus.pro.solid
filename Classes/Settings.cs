﻿using GuessGame_Solid.Interfaces;

namespace GuessGame_Solid
{

    public class Settings : ISettings
    {
        public int Min { get; }
        public int Max { get; }
        public int AttemptsNumber { get; }

        public Settings(int min, int max, int attemptsNumber)
        {
            Min = min;

            Max = max;

            AttemptsNumber = attemptsNumber;
        }
    }
}
