﻿namespace GuessGame_Solid.Interfaces
{
    public interface IOutput
    {
        public void Print(string str);

    }
}
