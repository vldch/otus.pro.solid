﻿using System;

namespace GuessGame_Solid.Interfaces
{
    public class ConsolFromIO : InputOutput
    {
        public override void Print(string str)
        {

            Console.WriteLine(str);

        }

        public override string ReadLine()
        {

            return Console.ReadLine();

        }
    }
}
