﻿using GuessGame_Solid.Interfaces;

namespace GuessGame_Solid.Classes
{

    public class Generator
    {
        public int GeneratedNumber { get; }

        public Generator(ISettings settings, IRandom rnd)
        {

            GeneratedNumber = rnd.Next(settings.Min, settings.Max + 1);
        
        }
    }
}
