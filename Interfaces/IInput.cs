﻿namespace GuessGame_Solid.Interfaces
{
    public interface IInput
    {
        public string ReadLine();

    }
}
