﻿using GuessGame_Solid.Interfaces;

namespace GuessGame_Solid.Classes
{
    public static class GameFactory
    {
        private static readonly Game Game;

        static GameFactory()
        {
            
            var settings = new Settings(0, 100, 5);
            
            var inputOutput = new ConsolFromIO();
            
            Game = new Game(settings, inputOutput);
        }

        public static Game GetInstance()
        {
            return Game;
        }

    }
}
